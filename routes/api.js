const express = require('express');
const router = new express.Router();
const Pantry = require('../models/Pantry');
const Food = require('../models/Food');
const Axios = require('axios');
const schedule = require('node-schedule');


/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

router.post('/pantry', async (req, res, next) => {
  try {
    const token = req.headers['authorization'];
    const response = await Axios.get(process.env.ACCOUNT_SERVICE_URL + '/api/user', {
      headers: {
        Authorization: token,
      },
    });
    console.log('response', response);
    if (response.data.user == null) {
      throw new Error('User not found');
    }
    const newPantry = new Pantry({
      userId: response.data.user._id,
    });
    res.json({
      pantry: await newPantry.save(),
    });
  } catch (error) {
    next(error);
  }
});

router.get('/pantry', async (req, res, next) => {
  try {
    const token = req.headers['authorization'];
    const response = await Axios.get(process.env.ACCOUNT_SERVICE_URL + '/api/user', {
      headers: {
        Authorization: token,
      },
    });
    const user = response.data.user;
    let pantry = await Pantry.findOne({
      userId: user._id,
    }).populate('foods');

    if (pantry == null) {
      pantry = new Pantry({
        userId: user._id,
      });
      await pantry.save();
    }

    res.status(200);
    res.json({
      pantry: pantry,
    }).end();
  } catch (error) {
    console.log(error);
    next(error);
  }
});

router.post('/pantry/food', async (req, res, next) => {
  try {
    const token = req.headers['authorization'];
    const userResponse = await Axios.get(process.env.ACCOUNT_SERVICE_URL + '/api/user', {
      headers: {
        Authorization: token,
      },
    });
    const user = userResponse.data.user;
    const pantry = await Pantry.findOne({
      userId: user._id,
    });

    const foodName = req.body.name;
    const foodExpirationDate = Date(req.body.expirationDate);

    const food = await new Food({
      name: foodName,
      expirationDate: foodExpirationDate,
    }).save();

    if (pantry.foods == null) {
      pantry.foods = [];
    }
    pantry.foods.push(food._id);
    await pantry.save();


    const response = await Axios.post(process.env.RECIPIE_URL + '/api/recipies/search', {
      name: foodName,
    }, {
      headers: {
        Authorization: token,
      },
    });
    const job = schedule.scheduleJob(foodExpirationDate, (async (foodName) => {
      const response = await Axios.post(process.env.RECIPIE_URL + '/api/recipies/search', {
        name: foodName,
      }).bind(null, food.name);
    }));


    res.status(201).json({
      pantry: await Pantry.findOne({
        userId: user._id,
      }).populate('foods'),
    });
  } catch (error) {
    console.log(error);
    next(error);
  }
});

router.put('/pantry/food', async (req, res, next) => {
  try {
    const foodId = req.body.foodId;
    const name = req.body.name;
    const expirationDate = req.body.expirationDate;

    await Food.findOneAndUpdate({_id: foodId}, {
      name: name,
      expirationDate: expirationDate,
    });

    res.status(200).json({food: await Food.findById(foodId)}).end();
  } catch (error) {
    next(error);
  }
});

router.delete('/pantry/food', async (req, res, next) => {
  try {
    const token = req.headers['authorization'];
    const user = await Axios.get(process.env.ACCOUNT_SERVICE_URL + '/api/user', {
      headers: {
        Authorization: token,
      },
    });
    const pantry = await Pantry.findOne({
      userId: user._id,
    });
    const foodId = req.body.foodId;

    pantry.foods.forEach((id, i) => {
      if (id == foodId) {
        pantry.foods.splice(i, 1);
      }
    });

    await pantry.save();

    res.status(200).json({
      pantry: await Pantry.findOne({
        userId: user._id,
      }).populate('foods'),
    });
  } catch (error) {
    console.log(error);
    next(error);
  }
});
module.exports = router;
