const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const foodSchema = new Schema({
  name: {
    required: true,
    type: String,
  },
  expirationDate: {
    required: true,
    type: Date,
  },
  dateCreated: {
    type: Date,
    default: Date.now(),
  },
  dateUpdated: {
    type: Date,
    default: Date.now(),
  },
});

const Food = mongoose.model('Food', foodSchema);

module.exports = Food;
