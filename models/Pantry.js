const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const pantrySchema = new Schema({
  userId: {
    required: true,
    type: String,
  },
  foods: [{
    type: Schema.ObjectId,
    ref: 'Food',
  }],
  dateCreated: {
    type: Date,
    default: Date.now(),
  },
  dateUpdated: {
    type: Date,
    default: Date.now(),
  },
});

const Pantry = mongoose.model('Pantry', pantrySchema);

module.exports = Pantry;
